﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace AutoSwitchInput
{
    public partial class MainServer : ServiceBase
    {
        public MainServer()
        {
            InitializeComponent();
        }
        string filePath = @"D:\MyService.txt";
        protected override void OnStart(string[] args)
        {
            // Todo 
            // test service
            //
            using(FileStream fileStream = new FileStream(filePath,FileMode.Append))
            using(StreamWriter writer = new StreamWriter(fileStream))
            {
                writer.WriteLine($"service onStart{DateTime.Now}");
            }  
        }

        protected override void OnStop()
        {
            using (FileStream fileStream = new FileStream(filePath, FileMode.Append))
            using (StreamWriter writer = new StreamWriter(fileStream))
            {
                writer.WriteLine($"service onStop{DateTime.Now}");
            }
        }
    }
}
